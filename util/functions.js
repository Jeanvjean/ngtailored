var moment = require('moment')
const nodemailer = require("nodemailer");
let smtpTransport = require('nodemailer-smtp-transport');
let hbs = require('nodemailer-express-handlebars');
const User = require('../app/auth/User')
var request  = require('request');

const   smsusername = 'jybsus',
        smspassword = 'Abuja1234!',
        smsendpoint = 'http://www.quicksms1.com/api/sendsms.php'

        mailService = 'Mailjet',
        mailUser = '9f60e0d1dde45472c57631f4d6fe3aee',
        mailPass = '2f9803789dd837f6a4af1fc37db58c68';
        mailSecret = 'mailjet._domainkey.payconnect.ng',
        mailDomain = 'ngtailored.com';


        var options = {
            viewEngine: {
                extname: '.hbs',
                layoutsDir: 'views/email/',
                defaultLayout : 'main',
                partialsDir : 'views'
            },
            viewPath: 'views',
            extName: '.hbs'
        };

var today = moment().format('L')
var two_weeks = moment().add(14, 'days').calendar()


function Email(email,message) {

    let transporter = nodemailer.createTransport(smtpTransport({
        service: mailService,
        auth: {
            user: mailUser,
            pass: mailPass
        }
    }));

    transporter.use('compile', hbs(options));

    var mailOptions = {
        from      : 'Ngtailored <info@ngtailored.ng>',
        to        :  email,
        subject   : 'Order Notification',
        template  : 'message',
        context  : message
    };

    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log("using info@ngtailored.com");
            console.log('Message sent: ' + info.response);
        }
    });
}
function Mailer(email,message) {

    let transporter = nodemailer.createTransport(smtpTransport({
        service: mailService,
        auth: {
            user: mailUser,
            pass: mailPass
        }
    }));

    transporter.use('compile', hbs(options));

    var mailOptions = {
        from      : 'Ngtailored <info@ngtailored.com>',
        to        :  email,
        subject   : 'Order Notification',
        template  : 'user_mail',
        context  : message
    };

    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log("using info@ngtailored.ng");
            console.log('Message sent: ' + info.response);
        }
    });
}

module.exports = {
  today,
  two_weeks,
  Email,
  Mailer
}
