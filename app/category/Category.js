var mongoose = require('mongoose')
var { Schema } = mongoose

var catSchema = new Schema({
  image:{type:String,required:true},
  name:{type:String,required:true},
  products:[{type:Schema.ObjectId,ref:'Product'}],
  blog:[{type:Schema.ObjectId,ref:'blog'}]
})
var Category = mongoose.model('Category',catSchema)

module.exports = Category
