const Category = require('./category')
const Product  = require('../products/product')

module.exports = {
  add:async(req,res)=>{
    var {name} = req.body

    var category = new Category({
      name,
      image:req.file.filename
    })
    await category.save()
    req.flash('success','Category Added')
    res.redirect('back')
  },
  delete:async(req,res)=>{
    var cat_id = req.params.id

    var category = await Category.findById(cat_id)
    console.log(category)
    await category.remove()
    res.json({
      success:true,
      message:'deleted'
    })
  },
  get:async(req,res)=>{
    var category = await Category.find({})
    res.json({
      success:true,
      data:category
    })
  },
  cats:async(req,res)=>{
    var  cat_id  = req.params.id

    console.log(cat_id)

    //finding categories by id
    var category = await Category.findById(cat_id).populate('products')

    res.json(category)
  },
  create:async(req,res)=>{
    res.render('shop/create_category',{title:'Add Category'})
  }
}
