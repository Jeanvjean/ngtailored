const express = require('express')
var router = express.Router()
var multer = require('multer')
var path = require('path')

var storage = multer.diskStorage({
  destination: './public/cate_image',
  filename:function(req,file,cb){
    cb(null,'category' + '_' + Date.now() + '_' + path.extname(file.originalname))
  }
})

var upload = multer({
  storage:storage
}).single('image')

var categoryController = require('./categoryController')

router.route('/category')
  .get(categoryController.get)
  .post(upload,categoryController.add)

router.route('/category/:id/delete')
  .get(categoryController.delete)

router.route('/category/:id')
  .get(categoryController.cats)

router.route('/create_category')
  .get(categoryController.create)

  module.exports = router
