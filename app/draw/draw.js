const mongoose = require('mongoose')
const { Schema } = mongoose;
//
// var raffleSchema = new Schema({
//   won:[{type:Schema.Object_id,ref:'ticket'}],
//   drew_on:{type:Date},
//   paid:{type:Boolean,default:false},
//   tickets:[{type:Schema.ObjectId,ref:'ticket'}]
// })
//
// var Draw = mongoose.model('Draw',raffleSchema)
// module.exports = Draw

var raffleSchema = new Schema({
  tickets:[{type:Number}],
  created:{type:Date},
  won:[{type:Schema.ObjectId,ref:'ticket'}]
})
 var Draw = mongoose.model('Draw',raffleSchema)
 module.exports = Draw
