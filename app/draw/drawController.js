const Draw = require('./draw')
const Ticket = require('../ticket/ticket')

module.exports = {
  create:async(req,res)=>{
    var ticket = await Ticket.find({}).populate('ticket_id -bought_on -expires_on -buyer -')
    var draw = new Draw({
      created:Date.now()
    })
    console.log(ticket)
  },
  update:async(req,res)=>{
    var {draw_id} = req.params
    var draw = await Draw.findById(draw_id)
    var winner = req.body.winner
    draw.won.push(winner)
    draw.save()
    res.status(200).json({
      draw,
      message:'Congratulation You are one of the weeks winner'
    })
  }
}
