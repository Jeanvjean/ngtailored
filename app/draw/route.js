const express = require('express')
const router = require('express-promise-router')()

var drawController = require('./drawController')

router.route('/create')
  .post(drawController.create)

router.route('/new_draw/:draw_id')
  .put(drawController.update)

module.exports = router
