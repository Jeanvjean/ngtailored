var express = require('express')
var router = require('express-promise-router')()


var homeController = require('./homeController')

router.route('/')
  .get(homeController.get)

module.exports = router
