var Product = require('../products/product')
var Category = require('../category/category')
var Blog = require('../blog/blog')

module.exports = {
  get:async(req,res)=>{
    var c = await Category.find({})
    var p = await Product.find({}).populate('category')
    var b = await Blog.find({}).populate('category')
    res.render('home',{
      title:'Home',
      product:p,
      category:c,
      blog:b
    })
  },
}
