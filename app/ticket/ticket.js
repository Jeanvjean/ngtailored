const mongoose = require('mongoose')
const { Schema } = mongoose;

var ticketSchema = new Schema({
  price:{type:Number,default:300},
  buyer:{type:Schema.ObjectId,ref:'user'},
  ticket_id:{type:Number},
  bought_on:{type:Date},
  expires_on:{type:Date},
  isExpired:{type:Boolean,default:false}
})

var Ticket = mongoose.model('Ticket',ticketSchema)
module.exports = Ticket
