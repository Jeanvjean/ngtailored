const express = require('express')
const router = require('express-promise-router')()

const ticketController = require('./ticketController')

router.route('/create')
  .post(ticketController.create)

router.route('/buy_ticket')
  .put(ticketController.buy_ticket)

module.exports = router
