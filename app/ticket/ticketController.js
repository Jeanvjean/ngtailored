const Ticket = require('./ticket')
const User = require('../auth/User')


module.exports = {
  create:async(req,res)=> {
    var ticket = new Ticket(req.body)
    await ticket.save()
    res.status(200).json({
      ticket,
      message:'New Ticket Added for week ' + ticket.week
    })
  },
  buy_ticket:async(req,res)=>{
    var ticket_number = Math.round((Math.random() * 1000000))
    console.log(ticket_number)
    var user = await User.findById(req.body.buyer)

    var ticket = new Ticket({
      ticket_id:ticket_number,
      bought_on:Date.now(),
      expires_on:Date.now() + 7,
      buyer:req.body.buyer,
    })
    await ticket.save()

    user.tickets.push(ticket)

    await user.save()

    console.log(ticket)
    console.log(user)
    res.status(200).json({
      ticket,
      message:'you have purchased a ticket for this weekends raffle draw. ticket number :' + ticket.ticket_id + ' Good Luck'
    })
  }
}
