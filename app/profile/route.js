const express = require('express')
const router = require('express-promise-router')()



const profileController = require('./profileController')

const usersController = require('../auth/usersController')

const isNotAuthenticated = (req,res,next)=>{
    if (req.isAuthenticated()) {
        req.flash('error','Sorry!!!You are already logged in')
        res.redirect('/')
    }else {
        return next()
    }
}
const isAuthenticated = (req,res,next)=>{
    if (req.isAuthenticated()) {
        return next()
    }else {
        req.flash('error','Sorry!!! You have to be registered')
        res.redirect('/')
    }
}


router.route('/profile/:user')
  .get(isAuthenticated,profileController.profile)


module.exports = router
