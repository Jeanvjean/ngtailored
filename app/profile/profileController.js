const User = require('../auth/User');
const Order = require('../cart/order')


module.exports = {
  profile:async(req,res)=>{
    var {user} = req.params
    var user_profile = await User.findOne({email:user}).populate('products orders new_orders')

    var m_orders = await Order.find({designer:user_profile._id})

    var pending = []
    var complete = []

    for (var i = 0; i < user_profile.orders.length; i++) {
      if (!user_profile.orders[i].done) {
        // console.log()
        pending.push(user_profile.orders[i])
      }else if (user_profile.orders[i].done) {
        complete.push(user_profile.orders[i])
      }
    }
    for (var i = 0; i < user_profile.new_orders.length; i++) {
      if (!user_profile.new_orders[i].done) {
        // console.log()
        pending.push(user_profile.new_orders[i])
      }else if (user_profile.new_orders[i].done) {
        complete.push(user_profile.new_orders[i])
      }
    }

    // console.log('pending',pending)
    // console.log('complete',complete)

    // console.log(pending)
    // console.log(user_profile)
      res.render('auth/profile',{
        title:'Profile',
        data:user_profile,
        pending:pending,
        complete:complete
    })
    // res.json(m_orders)
  }
}
