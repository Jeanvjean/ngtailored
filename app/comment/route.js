const express = require('express')
const router = require('express-promise-router')()

var commentController = require('./commentController')
const isAuthenticated = (req,res,next)=>{
    if (req.isAuthenticated()) {
        return next()
    }else {
        req.flash('error','Sorry!!! Please Login')
        res.redirect('/')
    }
}

router.route('/comment/:id')
  .post(isAuthenticated,commentController.create)

  module.exports = router
