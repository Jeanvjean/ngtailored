const mongoose = require('mongoose');
const { Schema } = mongoose;


var commentSchema = new Schema({
  comment:{type:String,required:true},
  email:{type:String,required:true},
  name:{type:String,required:true},
  created:{type:Date},
  blog:{type:Schema.ObjectId,ref:'blog'}
})

var Comment = mongoose.model('Comment',commentSchema)

module.exports = Comment
