const Comment = require('./comment')
const Blog = require('../blog/blog')

module.exports = {
  create:async(req,res)=>{
    var {id} = req.params
    var { title,name,email,comment } = req.body
    var comment = new Comment({
      email,
      name,
      title,
      comment,
      blog:id
    })
    // console.log(comment)
    await comment.save()
    var b = comment.blog
    // console.log(b)
    var bl = await Blog.findById(b)
    // console.log(bl)
    bl.comments.push(comment)
    await bl.save()
    //
    req.flash('success','Comment Added')
    res.redirect('back')
  }
}
