const passport  = require('passport')
const jwrStrategy = require('passport-jwt').Strategy
const localStrategy = require('passport-local').Strategy
const { ExtractJwt }  = require('passport-jwt')
const { jwt_secret } = require('./config/config')
const User = require('./authentication/user')

passport.use(new jwrStrategy({
  jwtFromRequest:ExtractJwt.fromHeader('authorization'),
  secretOrKey: jwt_secret
},async(payload,done)=>{
  try {
    var user = await User.findById(payload.sub)
    if(!user){
      return done(null,false)
    }
    done(null,user)
  } catch (e) {
    done(e,false)
  }
}))

//local strategy

passport.use(new localStrategy({
  usernameField:'email',
},async(email,password,done)=>{
try {
  const user = await User.findOne({email})
  if(!user){
    return done(null,false)
  }
  const isMatch = await user.isValid(password)
  if(!isMatch){
    return done(null,false)
  }
  done(null,user)
} catch (e) {
  done(e,false)
}
}))
