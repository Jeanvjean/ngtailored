const express = require('express')
const router = require('express-promise-router')()


var cartController = require('./cartController')


const usersController = require('../auth/usersController')

const isNotAuthenticated = (req,res,next)=>{
    if (req.isAuthenticated()) {
        req.flash('error','Sorry!!!You are already logged in')
        res.redirect('/')
    }else {
        return next()
    }
}
const isAuthenticated = (req,res,next)=>{
    if (req.isAuthenticated()) {
        return next()
    }else {
        req.flash('error','Sorry!!! Please Login')
        res.redirect('/')
    }
}

router.route('/my_cart')
  .get(isAuthenticated,cartController.get)

router.route('/add_to_cart/:slog')
  .post(cartController.add_to_cart)

router.route('/cart/update/:id')
  .get(cartController.update)

router.route('/cart/clear')
  .get(cartController.clear)


router.route('/order/details/:id')
  .get(isAuthenticated,cartController.order_detail)

router.route('/pay/:trans_ref')
  .post(cartController.pay_now)

router.route('/admin/all_orders')
  .get(isAuthenticated,cartController.fetch_orders)

router.route('/shipped/:id')
  .get(cartController.shipped)

router.route('/delivered/:id')
  .get(cartController.delivered)

router.route('/recieved/:id')
  .get(cartController.confirm_recieved)

router.route('/update_location/:id')
  .get(cartController.update_location)
  .post(cartController.update_order)

  module.exports = router
