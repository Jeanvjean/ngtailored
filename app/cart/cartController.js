var Product = require('../products/product')
var _ = require('lodash')
var Cart = require('./cart')
var Order = require('./order')
var User = require('../auth/User')
const Payout = require('../payout/Payout')
var request = require('request')
// const {initializePayment, verifyPayment} = require('../../paystack')(request);
const functions = require('../../util/functions')


module.exports = {
  get:async(req,res)=>{
    var cart = req.session.cart
    if (req.session.cart && req.session.cart.length == 0) {
      delete req.session.cart
      res.redirect('back')
    }else {
      res.render('cart/cart',{title:'My Cart'})
    }
    // res.json(req.session.cart)
    // console.log(req.session.cart.length)
  },
  add_to_cart:async(req,res)=>{
    var { slog } = req.params
    var size = req.body.size
    // console.log(req.body)

    var product = await Product.findOne({slog:slog}).populate('designer')
    console.log(product)
    if (typeof req.session.cart == 'undefined') {
      req.session.cart = []
      req.session.cart.push({
        name:product.name,
        id:slog,
        qty:1,
        shipping:2000.00,
        size:size,
        designer:product.designer.email,
        price:parseFloat(product.price).toFixed(2),
        image:product.image
      })
    } else {
          var cart = req.session.cart;
          var newItem = true;
          if (cart) {
            for (var i = 0; i < cart.length; i++) {
              if (cart[i].id == slog) {
                cart[i].qty++;
                newItem = false;
                break;
            }
              if (newItem) {
                cart.push({
                  name:product.name,
                  id:slog,
                  qty:1,
                  shipping:2000,
                  size:size,
                  designer:product.designer.email,
                  price:parseFloat(product.price).toFixed(2),
                  image:product.image
                })
                break;
              }
            }
          }
        }

    // console.log(req.session.cart)
    req.flash('success','Product Added to your cart')
    res.redirect('back')
    // res.json(req.session.cart)
  },
  update:async(req,res)=>{
    var p = req.params.id
    var cart = req.session.cart
    var action = req.query.action

    for (var i = 0; i < cart.length; i++) {
      if (cart[i].id == p) {
        switch (action) {
          case 'add': cart[i].qty++;
            break;
          case 'sub':cart[i].qty--
          if (cart[i].qty < 1) cart.splice(i,1);
            break;
            case 'remove': cart.splice(i,1)
            break;
            if (cart.length == 0) ; delete req.session.cart;
              break;
          default: console.log('update problem')
        }
        break;
      }
    }
    req.flash('success','cart updated')
    res.redirect('back')
  },
  clear:async(req,res)=>{
    delete req.session.cart
    req.flash('success','Cart Cleared')
    res.redirect('back')
  },

  pay_now:async(req,res) =>{

    console.log(req.body)
    var trans_ref = req.params.trans_ref
    var cart = req.session.cart
    var o_number = Math.floor((Math.random() * 1000000000) + 1)

    for (var i = 0; i < cart.length; i++) {
      // console.log(cart[i])

      var d_email = cart[i].designer
      // console.log(d_email)
      if (cart[i].designer == d_email) {
        var dc = cart[i]
      }
      // console.log(d_email)
      var shipping_address = req.user.shipping_address
      // console.log(req.user)
      var user = req.user._id
      var d =await User.findOne({email:d_email})


      var designer = d._id

      var order = new Order({
        client:user,
        cart:dc,
        shipping_address:shipping_address,
        trans_ref:trans_ref,
        designer:designer,
        created:functions.today,
        expected_delivery_date:functions.two_weeks,
        amount:req.body.amount,
        order_number:o_number
      })
      // console.log( 'order' ,order)
      await order.save()

      //
      var auth_user = await User.findById(order.client)
      var d_user = await User.findById(order.designer)

      // functions.Email(d_email,order.order_number)
      // functions.Mailer(auth_user.email,order.cart.name)

      d_user.new_orders.push(order)
      auth_user.orders.push(order)

      // console.log(t,des)

      await d_user.save()

      await auth_user.save()

    }

    delete req.session.cart

    req.flash('success','transaction completed')
    res.redirect('/products')
  },

  order_detail:async(req,res)=>{
    var o_id = req.params.id
    var order = await Order.findById(o_id).populate('designer client')

    res.render('order/detail',{title:'Order Details',data:order})
  },
  update_location:async(req,res)=>{
    var o_id = req.params.id
    res.render('order/change_location',{title:'Change Order Location',order:o_id})
  },
  update_order:async(req,res)=>{
    var o_id = req.params.id
    var { location } = req.body

    var order = await Order.findById(o_id)

    // console.log(order)
    order.location = location
    await order.save()

    req.flash('success','Location Updated')
    res.redirect('/order/details/' + o_id)
    // res.json(order)
  },
  fetch_orders:async(req,res)=>{
    var orders = await Order.find({}).populate('client designer')
    var complete = [];
    var pending = [];

    for (var i = 0; i < orders.length; i++) {
      if (!orders[i].done) {
        pending.push(orders[i])
      }else if (orders[i].done) {
        complete.push(orders[i])
      }
    }
    res.render('order/index',{
      title:'All Orders',
      data:orders,
      pending,
      complete,
    })
  },
  shipped:async(req,res)=>{
    var o_id = req.params.id

    var o = await Order.findById(o_id)
    o.status = 'shipped'

    await o.save()

    req.flash('success','order status updated')
    res.redirect('back')
  },
  delivered:async(req,res)=>{
    var o_id = req.params.id

    var o = await Order.findById(o_id)
    o.status = 'delivered'

    await o.save()

    functions.Email(o.client.email,o.cart.name)

    req.flash('success','order status updated')
    res.redirect('back')
  },
  confirm_recieved:async(req,res)=>{
    var o_id = req.params.id

    var o = await Order.findById(o_id)

    var seller_id = o.designer

    var seller = await User.findById(seller_id)

    functions.Email(seller.email,o.cart.name)

    var payout = new Payout({
      designer:seller_id,
      amount:o.amount
    })

    await payout.save()

    var pay_out = payout.amount * 0.90

    console.log(pay_out)
    // seller.wallet += pay_out
    var init_amount = seller.pay_out
    var new_wallet = init_amount + payout

    seller.wallet = new_wallet

    await seller.save()

    o.done = true
    o.delivered_on = functions.today

    await o.save()
    // console.log(o)

    req.flash('success','order has been recieved')
    res.redirect('back')
  }
}
