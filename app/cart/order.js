const mongoose = require('mongoose')
var { Schema } = mongoose


var orderSchema = new Schema({
  client:{type:Schema.ObjectId,ref:'User'},
  cart:{type:Object,required:true},
  trans_ref:{type: String,required:true},
  designer:{type:Schema.ObjectId,ref:'User'},
  shipping_address:{type:String,required:true},
  status:{type:String,enum:['shipped','delivered','processing'],default:'processing'},
  done:{type:Boolean,default:false},
  location:{type:String},
  created:{type:String},
  delivered_on:{type:String},
  expected_delivery_date:{type:String},
  dispute:{type:String,enum:['opened','closed','none'],default:'none'},
  disputed:{type:Schema.ObjectId,ref:'Dispute'},
  amount:{type:String},
  order_number:{type:String,unique:true,required:true}
})

var Order = mongoose.model('Order',orderSchema)

module.exports = Order
