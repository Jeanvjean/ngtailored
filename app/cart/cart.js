// module.exports = function Cart(oldCart) {
//   this.items = oldCart.items || {};
//   this.totalQty = oldCart.totalQty || 0;
//   this.totalPrice = oldCart.totalPrice || 0;
//
//   // var cart = [];
//
//   this.add = function(item, id, size) {
//     var storedItem = this.items[id];
//     if (!storedItem) {
//       storedItem = this.items[id] = { item:item, qty: 0, price: 0, size: size };
//     }
//     storedItem.qty++;
//     storedItem.price = storedItem.item.price * storedItem.qty;
//     this.totalQty++;
//     this.totalPrice += storedItem.item.price;
//   }
//   this.generateArray = function() {
//     var arr = [];
//     for (var id in this.items) {
//       arr.push(this.items[id]);
//     }
//     return arr;
//   }
// };

const mongoose = require('mongoose')
const { Schema } = mongoose;


var cartSchema = new Schema({
  product:{type:Schema.ObjectId,ref:'Product'},
  qty:{type:String},
  size:{type:String,required:true},
  user:{type:Schema.ObjectId,ref:'User'}
})

Cart = mongoose.model('Cart',cartSchema)
module.exports = Cart
