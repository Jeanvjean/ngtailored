const Product = require('./product')
var User  =require('../auth/User')
var Category = require('../category/category')

module.exports = {
  get:async(req,res)=>{
    var product = await Product.find({})
    var categories = await Category.find({})
    res.render('shop',{title:'Products',products:product,categories:categories})
  },
  cat:async(req,res)=>{
    var cat_id = req.params.id
    var c = await Category.findById(cat_id).populate('products')
    var cats = await Category.find({})
    var p = c.products
    // console.log(p)
    res.render('shop/categories',{
      products:p,
      title:c.name,
      categories:cats
    })
  },
  details:async(req,res)=>{
    var product_id = req.params.id
    // console.log(product_id)
    var product = await Product.findById(product_id).populate('designer')
    var cat_id = product.category
    // console.log(cat_id)
    var category = await Category.findById(cat_id).populate('products')
    // console.log(category)
    res.render('shop/product_details',{title:'Details',product:product,related:category})
  },
  get_create:async(req,res)=>{
    var category = await Category.find({})
    var designer = await User.find({})
    console.log(designer)
    res.render('shop/add_product',{
      title:'Add Products',
      designer:designer,
      category:category
    })
  },
  add:async(req,res)=>{
    // console.log(req.files.filename)
    var { name,description,price,category,designer } = req.body  //designer = user id
    // console.log(user)
    var slog = 'product' + '-' + Math.floor((Math.random() * 1000000000) + 1)
    var product = new Product({
      name:req.body.name,
      description:req.body.description,
      price:req.body.price,
      category:req.body.category,
      designer:req.body.designer,
      slog:slog
    })
    if (req.files) {
      for (var i = 0; i < req.files.length; i++) {
        // console.log(req.files[i].filename)
        var images = req.files[i].filename
        // console.log(images)
        product.image.push(images)
      }
    } else {
      req.flash('error','You Need To Add A Product Image')
      res.redirect('back')
      return
    }
    // console.log(product)
    await product.save()
//save product to a category
    var cat_id = product.category
    var category = await Category.findById(cat_id)
    category.products.push(product)
    await category.save()

    //save product to a designer
    var user_id = product.designer
    var user = await User.findById(user_id)
    user.products.push(product)
    await user.save()

    req.flash('success','product added to shop')
    res.redirect('back')
  },
  update:async(req,res)=>{
    var {productId} = req.params
    var updateProduct = req.body

    var product = await Product.findByIdAndUpdate(productId,updateProduct)

    res.json({message:'update complete',data:product})
  },
  delete:async(req,res)=>{
    var {productId} = req.params

    var product = await Product.findById(productId)

    var designer = product.designer
    var category = product.category

    var cat = await Category.findById(category)


    var user = await User.findById(designer)

    product.remove()

    cat.product.pull(product)

    user.products.pull(product)

    await user.save()

    res.json({
      message:'deleted successfully'
    })
  }
  // prod_detail:async(req,res)=>{
  //   var { prod_id } = req.params
  //
  //   var product = await Product.findById(prod_id)
  //
  //   res.json(product)
  // }
}
