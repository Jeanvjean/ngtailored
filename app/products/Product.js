const mongoose = require('mongoose')
const { Schema } = mongoose;


var productSchema = new Schema({
  name:{type:String,required:true},
  image:[{type:String,required:true}],
  description:{type:String,required:true},
  slog:{type:String,required:true},
  designer:{type:Schema.ObjectId,ref:'User'},
  category:{type:Schema.ObjectId,ref:'Category'},
  color:{type:String},
  price:{type:Number,required:true}
})

var Product = mongoose.model('Product',productSchema)

module.exports = Product
