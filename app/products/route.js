const express = require('express')
const router = require('express-promise-router')()
var multer = require('multer')
var path = require('path')
// var fs = require('fs-extra')
// var mkdir = require('mkdirp')
// var resize = require('resize-img')

var storage = multer.diskStorage({
  destination:'./public/products',
  filename:function(req,file,cb){
    cb(null,file.fieldname + 'products' + '_' + Date.now()+ '_' + path.extname(file.originalname))
  }
})


var upload = multer({
  storage:storage
}).any()

const productController = require('./productController')

router.route('/products')
  .get(productController.get)
  .post(upload,productController.add)

router.route('/product/:id/update')
  .put(productController.update)

router.route('/product/:id/delete')
  .delete(productController.delete)

router.route('/product/:id/details')
  .get(productController.details)

router.route('/add_product')
  .get(productController.get_create)

router.route('/products/:id')
  .get(productController.cat)

// router.route('/product/:id/details')
//   .get(productController.prod_detail)

module.exports = router
