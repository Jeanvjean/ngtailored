var express = require('express');
var router = require('express-promise-router')()
var multer = require('multer')
var path = require('path')

var storage = multer.diskStorage({
  destination:'./public/blog',
  filename:function(req,file,cb){
    cb(null,file.fieldname + 'blog'+ '_' + Date.now()+ '_' + path.extname(file.originalname))
  }
})

const isAuthenticated = (req,res,next)=>{
    if (req.isAuthenticated()) {
        return next()
    }else {
        req.flash('error','Sorry!!! Please Login')
        res.redirect('/')
    }
}

var upload = multer({
  storage:storage
}).any()

var blogController =  require('./blogController')


router.route('/blog')
  .get(blogController.get)
  .post(isAuthenticated,upload,blogController.create)

router.route('/blog/delete/:id')
  .get(isAuthenticated,blogController.delete)

router.route('/blog/:id')
  .get(blogController.details)

router.route('/add_post')
  .get(isAuthenticated,blogController.create_blog)

router.route('/blog_cat')
  .get(isAuthenticated,blogController.blog_cat)
  .post(isAuthenticated,blogController.add_cat)

router.route('/blog/cat/:id')
  .get(blogController.filter)


module.exports = router
