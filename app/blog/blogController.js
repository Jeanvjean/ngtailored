var Cat = require('./cats')
var Blog = require('./blog')
var util = require('../../util/functions')
var Comment = require('../comment/comment')
var Product = require('../products/product')
var Category = require('../category/category')

module.exports = {
  create:async(req,res)=>{
    console.log(req.files)
    var { title,body,category} = req.body

    var blog = new Blog({
      title,
      body,
      category,
      created:util.today
    })

    if (req.files != undefined) {
      for (var i = 0; i < req.files.length; i++) {
        // console.log(req.files[i].filename)
        var images = req.files[i].filename
        // console.log(images)
        blog.picture.push(images)
      }
    }

    await blog.save()

    var cat = blog.category
    var cats = await Cat.findById(cat)
    cats.blogs.push(blog)

    await cats.save()

    req.flash('success','blog post added successfully')
    res.redirect('/blog')
  },
  get:async(req,res)=>{
    var b = await Blog.find({})
    var c = await Cat.find({})
    var p = await Product.find({})
    res.render('blog',{blog:b,title:'Blog',category:c,products:p})
  },
  details:async(req,res)=>{
      var {id} = req.params
      var blog = await Blog.findById(id).populate('comments')
      // console.log(blog)
      var cats = await Cat.find({})
      var prod = await Product.find({})
      // var c = await Comment.find({})
      res.render('blog/details',{
        title:'Details',
        products:prod,
        category:cats,
        blog:blog
    })
  },
  create_blog:async(req,res)=>{
    var cats = await Cat.find({})
    res.render('blog/create',{title:'Create Blog',category:cats})
  },
  delete:async(req,res)=>{
    var b_id = req.params.id
    var blog = await Blog.findById(b_id)
    var b_cat = await Cat.findById(blog.category)

    await blog.remove()

    b_cat.blogs.pull(blog)


    await b_cat.save()

    req.flash('success','blog post Deleted')
    res.redirect('back')
  },
  blog_cat:async(req,res)=>{
    res.render('blog/create_cat',{title:'Add Blog Categories'})
  },
  add_cat:async(req,res)=>{
    var cat = new Cat({
      name:req.body.name
    })
    await cat.save()

    req.flash('success','Category Added')
    res.redirect('/add_post')
  },
  filter:async(req,res)=>{
    var cat_id = req.params.id

    var c = await Cat.find({})
    var p = await Product.find({})

    var bc = await Cat.findById(cat_id).populate('blogs')

    res.render('blog/blog_cats',{
        title:c.name + 'Blog Posts',
        blog:bc.blogs,
        category:c,
        products:p,
     })
    // res.send(bc)
  }
}
