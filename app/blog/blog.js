const mongoose = require('mongoose');
const { Schema } = mongoose;

var blogSchema = new Schema({
  title:{type:String,required:true},
  body:{type:String,required:true},
  picture:[{type:String}],
  comments:[{type:Schema.ObjectId,ref:'Comment'}],
  category:{type:Schema.ObjectId,ref:'Cat'},
  created:{type:String}
})

var Blog = mongoose.model('Blog',blogSchema)

module.exports = Blog
