const mongoose = require('mongoose')
const { Schema } = mongoose;

var catSchema = new Schema({
  name:{type:String,required:true},
  blogs:[{type:Schema.ObjectId,ref:'Blog'}]
})

var Cat = mongoose.model('Cat',catSchema)
module.exports = Cat
