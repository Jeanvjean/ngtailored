const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const User = require('../User')
const bcrypt = require('bcryptjs')

//passport serialize
passport.serializeUser((user,done)=>{
    done(null,user.id)
})
//passport deserialize
passport.deserializeUser(async(id,done)=>{
    try {
        const user = await User.findById(id)
        done(null,user)
    } catch (e) {
        done(e,null)
    }
})


//passport authenticate
passport.use('local', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallBack: false
}, async(email,password,done)=>{
    try {
        const user = await User.findOne({'email':email})
        if (!user) {
            return done(null,false,{message: 'Unknown User'})
        }
        console.log(await bcrypt.compare(password,user.password))
        const isMatch =await bcrypt.compare(password,user.password)
        // console.log(isMatch)
        if (isMatch) {
            return done(null,user,{message:'Login Successfull'})
        }
        return done(null,false,{message:'invalid password'})
    } catch (e) {
        done(e,false)
    }
}))
