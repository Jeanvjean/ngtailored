const express = require('express')
const router = require('express-promise-router')()
const multer = require('multer')
var path = require('path')

const usersController = require('./usersController')

const isNotAuthenticated = (req,res,next)=>{
    if (req.isAuthenticated()) {
        req.flash('error','Sorry!!!You are already logged in')
        res.redirect('/')
    }else {
        return next()
    }
}
const isAuthenticated = (req,res,next)=>{
    if (req.isAuthenticated()) {
        return next()
    }else {
        req.flash('error','Sorry!!! Please Login')
        res.redirect('/')
    }
}

var storage = multer.diskStorage({
  destination: './public/profile_img',
  filename: function(req,file,cb){
    cb(null,file.fieldname + 'profile_image' + '_' + Date.now()+ '_' + path.extname(file.originalname))
  }
})

var upload = multer({
  storage:storage
}).single('profile_image')


    router.route('/register')
        .get(isNotAuthenticated,usersController.getRegister)
        .post(usersController.register)

    router.route('/login')
        .get(isNotAuthenticated,usersController.getLogin)
        .post(usersController.login)

    router.route('/logout')
        .get(isAuthenticated,usersController.logout)

    router.route('/update_profile/:user')
      .get(isAuthenticated,usersController.update_p)

    router.route('/update/:user')
    .post(isAuthenticated,upload,usersController.update)

    router.route('/recover_password')
      .get(usersController.get_pass)

    router.route('/recovery_email')
      .post(usersController.forgot_password)

    router.route('/change_password')
      .post(usersController.change_password)

        module.exports = router
