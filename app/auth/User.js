const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const { Schema } = mongoose


const userSchema = new Schema({
    firstname:{type:String,required:true},
    lastname:{type:String,required:true},
    company:{type:String},
    code:{type:Number,default:0},
    company_desc:{type:String},
    shipping_address:{type:String},
    email:{type:String, required:true, unique:true, lowercase:true},
    password:{type:String, required:true},
    phone:{type:Number},
    products:[{type:Schema.ObjectId,ref:'Product'}],
    account_type:{type:String,default:'client'},
    profile_image:{type:String},
    cart:[{type:Schema.ObjectId,ref:'Product'}],
    address:{type:String},
    account_number:{type:String},
    account_name:{type:String},
    bank:{type:String},
    state:{type:String},
    country:{type:String},
    orders:[{type:Schema.ObjectId,ref:'Order'}],
    new_orders:[{type:Schema.ObjectId,ref:'Order'}],
    payouts:[{type:Schema.ObjectId,ref:'Payout'}],
    wallet:{type:String,default:0}
    })
userSchema.pre('save', async function(next){
    try {
        //gen salt
        const salt =  await bcrypt.genSalt(10)
        //gen hash
        const hash = await bcrypt.hash(this.password,salt)
        //hash password
        this.password = hash
        next()
    } catch (e) {
        next(e)
    }
})
const User = mongoose.model('User',userSchema)

module.exports = User
