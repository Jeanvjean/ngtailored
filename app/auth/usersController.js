const User = require('./User')
const Joi = require('joi')
var bcrypt = require('bcryptjs')
const passport = require('passport')
const functions = require('../../util/functions')




//============================================= validation==========================================//
const userSchema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().regex(/^[a-zA-Z0-9]{6,32}/).required()
})
//===============================================validation========================================//
module.exports = {
    //============================================registeration=======================================//
    getRegister: async(req,res)=>{
        res.render('auth/signup',{title:'Register'})
    },
    register: async(req,res,next)=>{
        const result = Joi.validate(req.body,userSchema)
        // console.log(result)
        const user = await User.findOne({email:result.value.email})
        // console.log(user)

        if (user) {
            req.flash('error', 'this user already exists')
            res.redirect('/register')
            // res.send('user already exist')
            return
        }
        const newUser = new User(result.value)
        if (result.value.account_type === 'admin') {
          newUser.account_type = 'admin'
        } else if (result.value.account_type === 'client') {
          newUser.account_type = 'client'
        } else if (result.value.account_type === 'designer') {
          newUser.account_type = 'designer'
        }
        // console.log(newUser)
        await newUser.save()

        req.flash('success', 'Registeration Successfull')
        res.redirect('/login')
        // res.send(newUser)
    },
//============================================registeration=======================================//
//============================================Login==============================================//
    login:     passport.authenticate('local',{
                successRedirect: '/',
                failureRedirect: '/login',
                failureFlash: true
            }),
    getLogin: async(req,res)=>{
        res.render('auth/signin',{title:'Signin'})
    },
    logout: (req,res)=>{
        req.logout()
        req.flash('success', 'You are Logged out')
        res.redirect('/')
    },
//============================================Login==============================================//

  update:async(req,res)=>{
    var {user} = req.params
    // console.log(req.files)
    // var profile_image = req.files.filename
    var newU = {
      firstname:req.body.firstname,
      lastname:req.body.lastname,
      company:req.body.company,
      shipping_address:req.body.shipping_address,
      state:req.body.state,
      country:req.body.country,
      phone:req.body.phone,
      company_desc:req.body.company_desc,
      address:req.body.address,
      bank:req.body.bank,
      account_name:req.body.account_name,
      account_number:req.body.account_number,
      // profile_image:req.file.filename
    }
    if (req.file) {
      newU.profile_image = req.file.filename
    }
    var u = await User.findByIdAndUpdate(user,newU)
    // console.log(u)
    // updated = await u.update(u,newU)
    req.flash('success','Your Profile has been updated !!!')
    res.redirect('back')
  },
  update_p:async(req,res)=>{
    var {user} = req.params

    var u = await User.findById(user)

    res.render('auth/update_profile',{title:'Profile Update', data:u})
  },
  forgot_password:async(req,res)=>{
    var {email} = req.body
    var code = Math.round(Math.random() * 10000)
    var user = await User.findOne({email:email})

    user.code = code

    await user.save()

    // functions.Email(email,code)

    req.flash('success','A recovery code was sent to your email')
    res.render('auth/change_password',{title:'Recovery Email'})
  },
  get_pass:async(req,res)=>{
    res.render('auth/password',{title:'Recovery Email'})
  },
  change_password:async(req,res)=>{
    var {code,password,email} = req.body

    var user = await User.findOne({email})
    if (!user) {
      return req.flash('error','This User Does Not Exist check the email')
    }else {
      // console.log(user)
      if (user.code == code) {

        user.password = password
        // console.log(user)
        await user.save()

        req.flash('success','Password Change Successfull')
        res.redirect('/login')
      }else {
        req.flash('error','Invalid recovery code')
        return res.redirect('back')
      }
    }
  }
}
