const Dispute = require('../dispute/Dispute')
const Order = require('../cart/order')
const Time = require('../../util/functions')
const User = require('../auth/User')


module.exports = {
  create:async(req,res)=>{
    var o_id = req.params.id
    var order = await Order.findById(o_id)
    res.render('dispute/create',{title:'Open Dispute',order:order})
  },
  start:async(req,res)=>{
    var order_id = req.params.id
    var order = await Order.findById(order_id)


    // var seller = await User.findById(order.designer)
    // console.log(seller)
    var complain = new Dispute({
      product:order,
      complainer:order.client,
      complaint:req.body.complain,
      seller:order.designer,
      created:Time.today
    })
    await complain.save()

    order.disputed = complain
    order.dispute = 'opened'

    await order.save()

    req.flash('success','Opened dispute')
    res.redirect('back')
  },
  reply:async(req,res)=>{
    var d_id = req.params.id
    var message = req.body.message

    var dispute = await Dispute.findById(d_id)

    var user = req.user.email

    var reply = {
      repliedBy:user,
      message:message,
      repliedOn:Time.today
    }

    dispute.reply.push(reply)

    await dispute.save()

    req.flash('success','Done')
    res.redirect('back')
  },
  details:async(req,res)=>{
    var d_id = req.params.id

    var dispute = await Dispute.findById(d_id).populate('product client seller')

    res.render('dispute/details',{title:'Dispute',dispute:dispute})
    // res.json(dispute)
  }
}
