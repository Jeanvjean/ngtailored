const mongoose = require('mongoose')
const {Schema} = mongoose

var disputeSchema = new Schema({
  product:{type:Schema.ObjectId,ref:'Order'},
  complainer:{type:Schema.ObjectId,ref:'User'},
  complaint:{type:String,required:true},
  seller:{type:Schema.ObjectId,ref:'User'},
  settled:{type:Boolean,default:false},
  created:{type:String,required:true},
  reply:[{
    repliedBy:{type:String},
    message:{type:String},
    repliedOn:{type:String}
  }]
})

var Dispute = mongoose.model('Dispute',disputeSchema)

module.exports = Dispute
