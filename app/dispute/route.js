const express = require('express')
const router = require('express-promise-router')()

const isAuthenticated = (req,res,next)=>{
    if (req.isAuthenticated()) {
        return next()
    }else {
        req.flash('error','Sorry!!! Please Login')
        res.redirect('/')
    }
}

const disputeController = require('./disputeController')

router.route('/dispute/:id')
  .get(isAuthenticated,disputeController.create)
  .post(isAuthenticated,disputeController.start)

router.route('/dispute/reply/:id')
  .post(isAuthenticated,disputeController.reply)

router.route('/details/:id/dispute')
  .get(isAuthenticated,disputeController.details)

  module.exports = router
