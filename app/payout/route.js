const express = require('express')
const router = require('express-promise-router')()


var payoutController = require('./payoutController')

router.route('/payouts')
  .get(payoutController.all)

module.exports = router
