const User = require('./user')
const jwt = require('jsonwebtoken')
const {jwt_secret} = require('../config/config')
signToken = (user)=>{
  return jwt.sign({
    iss:'tailored',
    sub:user.id,
    iat:new Date().getTime(),
    exp: new Date().setDate(new Date().getDate() + 1)
  },jwt_secret)
}
module.exports = {
  create:async(req,res)=>{
    console.log(req.value)
    var newUser = new User(req.value.body)
    var foundUser = await User.findOne({email:newUser.email})
    if (foundUser) {
      return res.status(400).json({
        message:'this user already exists,please login or retrieve password'
      })
    }
    await newUser.save()
    const token = signToken(newUser)
    res.status(200).json({
      user:newUser,
      token:token,
      message:'registration successful'
    })
  },
  login:async(req,res)=>{
    var token = signToken(req.user)
    var user = req.user
    res.status(200).json({
      user,
      token,
      message:'login,successful'
    })
  },
  secret:async(req,res)=>{
    res.status(200).json({
      message:'this is a secrete component'
    })
  }
}
