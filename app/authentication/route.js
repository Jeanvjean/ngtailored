const express = require('express')
const router = require('express-promise-router')()
const passport = require('passport')
const pasConf = require('../passport')

const userController = require('./userController')
const {validateBody,schemas} = require('../../helpers/routeHelpers')

router.route('/user')
  .post(validateBody(schemas.authSchema),userController.create)

router.route('/login')
  .post(passport.authenticate('local',{session:false}),userController.login)

router.route('/secret')
  .get(passport.authenticate('jwt',{session:false}),userController.secret)

module.exports = router
