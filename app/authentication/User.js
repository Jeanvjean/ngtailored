const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const {Schema} = mongoose;

const userSchema = new Schema({
  firstname:{type:String},
  lastname:{type:String},
  email:{type:String,required:true,unique:true},
  password:{type:String,required:true},
  phone :{type:Number,required:true},
  profile_updated:false,
  country:{type:String},
  products:[{type:Schema.ObjectId,ref:'Product'}],
  isVerified:{type:Boolean,default:false},
  account_type:{type:String,default:'user'},
  verification_token:{type:String}
})
userSchema.pre('save',async function(next){
  try {
    const salt = await bcrypt.genSalt(8)
    const hash = await bcrypt.hash(this.password,salt)
    this.password = hash
    next()
  } catch (e) {
    next(e)
  }
})
userSchema.methods.isValid = async function(newPassword){
  try {
    return await bcrypt.compare(newPassword,this.password)
  } catch (e) {
    console.log(e)
  }
}
const User = mongoose.model('User',userSchema)

module.exports = User
