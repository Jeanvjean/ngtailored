const express = require('express')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const mongoose = require('mongoose')
var fileUpload = require('express-fileupload')
const ejs = require('ejs')
const flash = require('connect-flash')
var cookieParser = require('cookie-parser')
const session = require('express-session')
const messages = require('express-messages')
var request = require('request')
const passport = require('passport')
const MongoStore = require('connect-mongo')(session);

const user = require('./app/auth/route')
const ticket = require('./app/ticket/route')
const draw = require('./app/draw/route')
const product = require('./app/products/route')
const category = require('./app/category/route')
const home = require('./app/home/route')
const cart = require('./app/cart/route')
const blog = require('./app/blog/route')
const profile = require('./app/profile/route')
const comment = require('./app/comment/route')
const dispute = require('./app/dispute/route')
const payout = require('./app/payout/route')

// const _ = require('lodash');
// const path = require('path');
// // const {Donor} = require('./models/donor')
// const {initializePayment, verifyPayment} = require('./paystack')(request);

require('./app/auth/config/passport')

module.exports = (app)=>{
  mongoose.connect('mongodb://localhost:27017/tailored',{useNewUrlParser:true},(e,con)=>{
    if (e) {
      console.log(e)
      return;
    }
     console.log('Connected to MongoDB...')
  })

  //set view engine
  app.set('view engine','ejs')

  app.use(express.static(__dirname + '/public'))

  app.use(cors())
  app.use(morgan('dev'))
  // app.use(fileUpload())
  app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({extended:false}))
    app.use(cookieParser())
    app.use(session({
        secret:'secret',
        saveUninitialized:false,
        store: new MongoStore({ mongooseConnection: mongoose.connection }),
        cookie:{maxAge:180 * 60 * 100},
        resave:false
    }))
    app.use(passport.initialize())
    app.use(passport.session())

    app.use(flash())

    app.use((req,res,next)=>{
        res.locals.success_msg = req.flash('success')
        res.locals.error_msg = req.flash('error')
        res.locals.isAuthenticated = req.user ? true : false
        // req.user ? true : false
        next()
    })

  app.get('*',(req,res,next)=>{
    res.locals.cart = req.session.cart;
    res.locals.user = req.user || null;
    next();
  })

  app.use(user)
  app.use(blog)
  app.use(profile)
  app.use(dispute)
  app.use('/ticket',ticket)
  app.use('/draw',draw)
  app.use(product)
  app.use(comment)
  app.use(category)
  app.use(home)
  app.use(cart)
  app.use(payout)
  
  app.use(function(req,res){
  res.status(404).render('404',{title:'404 page not found'})
})
}
