const joi = require('joi')

module.exports = {
  validateBody:(schemas)=>{
    return (req,res,next)=>{
      const result = joi.validate(req.body,schemas)
      if(result.error){
        return res.status(400).json(result.error)
      }
      if(!req.value){req.value = {};}
      req.value['body'] = result.value;
      next()
    }
  },
  schemas: {
    authSchema: joi.object().keys({
      email:joi.string().email().required(),
      firstname:joi.string(),
      lastname:joi.string(),
      phone : joi.number().required(),
      password:joi.string().required()
    })
  }
}
